/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import ComponentReducer from './src/cmpreducer'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import allReducer from './src/reducer'
import thunk from 'redux-thunk'
import TaskFlastList from './TaskFlatList '
import AddView from './AddView'
import CouterCmp from './src/cmpreducer'
import { persistor, store } from './src/store/index'
import { PersistGate } from 'redux-persist/integration/react'



export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <CouterCmp />
        </PersistGate>
      </Provider>
    );
  }



/*

//SU DUNG REDUX VOI FLASH LIST

//DATA
let appState = {
  data: [{
    tittle: 'anhanh', isFinished: true
  },
  {
    tittle: '1234', isFinished: false
  },
  ]
}

let test = {
  data: []
}

//ACTION

const FinishTask = (index) => {
  return {
    type: 'FINISH',
    atIndex: true
  }
}

const DELETE = (index) => {
  return {
    type: 'DELETE',
    atIndex: true
  }
}


//REDUCER
const taskListReducer = (state = appState, action) => {
  let NewTaskList = state.data;
  switch (action.type) {
    case 'ADD':
      let NewTask = { tittle: action.TaskName, isFinished: false }
      return { ...state, data: [...state.data, NewTask] }

    case 'FINISH':

      NewTaskList[action.atIndex].isFinished = true

      return { ...state, data: NewTaskList }
    case 'DELETE':
      NewTaskList = NewTaskList.filter((item, i) => i !== action.atIndex)
      return { ...state, data: NewTaskList }


  }

  return state
}

const testgetthunk = (state = test, action) => {
  //let newThun = state.data;
  switch (action.type) {
    case 'TEST_THUNK':
      return { ...state, data: action.data }
  }
  return state;
}

//Store

const reducers = combineReducers({ task: taskListReducer, thunk: testgetthunk })
const store = createStore(reducers, applyMiddleware(thunk))

//const store = createStore(taskListReducer, appState)

//let store = createStore(allReducer);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AddView />
        <TaskFlastList />
      </Provider>

    );
  }
}


*/
