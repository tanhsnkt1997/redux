import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Platform,
    TextInput,
    Text,
    TouchableOpacity,
    FlatList,
    View
} from 'react-native';
import { connect } from 'react-redux'
import axios from 'axios';


class TaskFlatList extends Component {

    renderItem = ({ item, index }) => {

        const { onFinishedItem, onDeleteItem, testThunk } = this.props;

        return (
            <View style={styles.itemContainer}>
                <View>
                    <TouchableOpacity style={{ marginTop: -2 }} onPress={() => onFinishedItem(index)}>
                        <Text> {(item.isFinished) ? `✅` : `🕘`} </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ color: 'black' }}>{item.tittle}</Text>
                </View>
                <View style={{ justifyContent: 'center' }}>
                    <TouchableOpacity style={{ marginTop: -2 }} onPress={() => testThunk()}>
                        <Text>{`❌`}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }


    render() {
        console.log('PROPS la: =============>', this.props)
        console.log('Data la: ======>', this.props.listData)
        const { data } = this.props.listData.task
        return (
            <FlatList
                data={data}
                extraData={this.props}
                keyExtractor={(item, index) => index}
                renderItem={this.renderItem}
            />
        );
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        marginHorizontal: 10,
        marginTop: 10,
        paddingHorizontal: 10,
        paddingVertical: 15,
        borderRadius: 5,
        borderColor: 'gray',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        shadowColor: 'gray',
        elevation: 2
    }
});

//ACTION

const FinishTask = (index) => {
    return {
        type: 'FINISH',
        atIndex: index
    }
}

const DELETE = (index) => {
    return {
        type: 'DELETE',
        atIndex: index
    }
}

const testThunk = () => {
    return (dispatch) => {
        axios.get(`https://jsonplaceholder.typicode.com/users`)
            .then(data => {
                dispatch(
                    {
                        type: 'TEST_THUNK',
                        data: data.data
                    }
                )
            })
    }
}

export default connect(
    state => {
        return {
            //  listData: state.data
            listData: state
        }
    },
    dispatch => {
        return {
            onFinishedItem: (index) => dispatch(FinishTask(index)),
            onDeleteItem: (index) => dispatch(DELETE(index)),
            testThunk: () => dispatch(testThunk())
        }
    }
)(TaskFlatList)