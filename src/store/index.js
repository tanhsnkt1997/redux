import { createStore } from 'redux'
import reducer from '../reducer/index'
import { AsyncStorage } from 'react-native';
import { persistStore, persistReducer } from 'redux-persist'


const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}

const prReducer = persistReducer(persistConfig, reducer)



export const store = createStore(prReducer);
export const persistor = persistStore(store);